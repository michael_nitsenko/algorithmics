#!/usr/bin/env python3

from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
import matplotlib.animation as animation

ptr = 0



import pyaudio
import wave
import time
import sys
import struct
import threading

wf = None
pa = None
stream = None
shorts = []
forest = []
pointer = [0, 0]
t = None
t1 = None
diffs = []
hop = 1

class Scope(object):
	def __init__(self, ax, maxt=100, dt=0.02):
		self.ax = ax
		self.dt = dt
		self.maxt = maxt
		self.tdata = [0]
		self.ydata = [0]
		self.line = Line2D(self.tdata, self.ydata)
		self.ax.add_line(self.line)
		self.ax.set_ylim(-32768, 32767)
		self.ax.set_xlim(0, self.maxt)

	def update(self, y):
		lastt = self.tdata[-1]
		if lastt > self.tdata[0] + self.maxt:  # reset the arrays
			self.tdata = [self.tdata[-1]]
			self.ydata = [self.ydata[-1]]
			self.ax.set_xlim(self.tdata[0], self.tdata[0] + self.maxt)
			self.ax.figure.canvas.draw()

		t = self.tdata[-1] + self.dt
		self.tdata.append(t)
		self.ydata.append(y)
		self.line.set_data(self.tdata, self.ydata)
		return self.line,


def emitter():
	global shorts, ptr
	if ptr > len(shorts) - 1: yield 0
	ptr += 1
	yield shorts[ptr - 1]


fig, ax = plt.subplots()
scope = Scope(ax)

ani = animation.FuncAnimation(fig, scope.update, emitter, interval=100, blit=True)




# ALGO

def onlySpikes(arr):
	n_arr = []
	for i in range(len(arr)):
		if i > 0 and i < len(arr) - 1:
			if arr[i - 1] <= arr[i] and arr[i] > arr[i + 1]:
				n_arr.append(arr[i])

	return n_arr

class Tree:
	value = 0
	children = []

def readLookup():
	global forest, hop

	file = open('./resources/cuts', 'r')
	for line in file:
		values = line.split(',')
		values = list(map(lambda x: int(x), values))
		values = onlySpikes(values)
		print(len(values))
		tree = None
		equal = len(forest) > 0
		hopped = 0
		for value in values:
			if hopped < hop - 1:
				hopped += 1
				continue
			hopped = 0
			int_v = value
			if tree == None:
				for trie in forest:
					if trie.value == int_v: tree = trie
					else: equal = False
			if equal == False:
				next_tree = Tree()
				next_tree.value = int_v
				if tree == None:
					forest.append(next_tree)
				else:
					next_tree.children.append(next_tree)

def travers():
	global shorts, stream, forest, pointer, hop
	while stream == None:
		time.sleep(0.01)
		continue

	while (pointer[0] < len(shorts) and stream == None) or (not stream == None and stream.is_active()):
		if pointer[0] > len(shorts) - 1:
			time.sleep(0.01)
			continue

		subforest = forest
		valid = True
		dead_end = False
		while valid:
			valid = False
			if pointer[1] > len(shorts) - 1:
				time.sleep(0.01)
				continue
			
			for i_tree in subforest:
				if i_tree.value == shorts[pointer[1]]:
					valid = True
					pointer[1] += hop
					subforest = i_tree.children
					if len(subforest) == 0:
						valid = False
						dead_end = True
					break

			if dead_end:
				pointer[0] = pointer[1]
				print("----------Found")
			elif not valid:
				diff = pointer[1] - pointer[0]
				if(diff > 100):
					print("----------Partial Match")
					print(pointer[0], pointer[1] - pointer[0])
				pointer[0] += hop
				pointer[1] = pointer[0]

def startLookup():
	global t
	t = threading.Thread(target=travers)
	t.start()

def formatBlock(data):
	count = len(data) / 2
	format = "%dh"%(count)
	shorts_part = struct.unpack(format, data)
	return shorts_part

# total 1700352 points, 26 sec => 0.00001529 sec spacing, 44,1 kHz, 3.4 MB
# cutted 566514 points => 0.00004589
def callback(in_data, frame_count, time_info, status):
	global wf, pa, shorts, pointer, diffs
	data = wf.readframes(frame_count)
	shorts_part = formatBlock(data)
	shorts_part = onlySpikes(shorts_part)
	shorts.extend(shorts_part)
	diff = len(shorts) - pointer[0]
	diffs.append(diff)
	print(diff)
	return (data, pyaudio.paContinue)


def startStream():
	global wf, pa, stream, diffs
	wf = wave.open('./resources/Esskeetit.wav', 'rb')
	pa = pyaudio.PyAudio()
	stream = pa.open(format=pa.get_format_from_width(wf.getsampwidth()),
					channels=wf.getnchannels(),
					rate=wf.getframerate(),
					output=True,
					stream_callback=callback)
	stream.start_stream()
	while stream.is_active():
		time.sleep(0.1)

	print(diffs)
	stream.stop_stream()
	stream.close()
	stream = None
	wf.close()
	pa.terminate()


def main():
	global t1
	readLookup()
	t1 = threading.Thread(target=startStream)
	t1.start()
	startLookup()


if __name__ == "__main__":
	main()
	#plt.show()