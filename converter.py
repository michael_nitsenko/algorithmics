#!/usr/bin/env python3

import pyaudio
import wave
import time
import sys
import struct
import math

wf = None
pa = None
shorts = None
file = open('./raw', 'w+')

def formatBlock(data):
    if len(data) % 2 > 0: return []
    count = len(data)/2
    format = "%dh"%(count)
    shorts_pack = struct.unpack(format, data)
    return shorts_pack

def write_to_file():
    global shorts
    part = ''
    max_sh = -1
    min_sh = 1
    for short in shorts:
        if len(part) > 0: part += ','
        part += str(short)
        if max_sh < short: max_sh = short
        if min_sh > short: min_sh = short

    print(min_sh, max_sh)
    file.write(part + '\n')

def callback(in_data, frame_count, time_info, status):
    global wf, pa, shorts
    data = wf.readframes(frame_count)
    shorts_pack = formatBlock(data)
    shorts.extend(shorts_pack)
    return (data, pyaudio.paContinue)

def startStream(resource):
    global wf, pa, shorts
    shorts = []
    print(len(shorts))
    wf = wave.open(resource, 'rb')
    pa = pyaudio.PyAudio()
    stream = pa.open(format=pa.get_format_from_width(wf.getsampwidth()),
                    channels=wf.getnchannels(),
                    rate=wf.getframerate(),
                    output=True,
                    stream_callback=callback)
    stream.start_stream()
    while stream.is_active():
        time.sleep(0.1)

    stream.stop_stream()
    stream.close()
    wf.close()
    pa.terminate()
    print(len(shorts))
    write_to_file()


def main():
    global file
    startStream('./resources/Esskeetit.wav')

    file.close()


if __name__ == "__main__":
    main()