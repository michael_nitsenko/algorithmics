# code by markjay4k was used as an example: https://github.com/markjay4k/Audio-Spectrum-Analyzer-in-Python/blob/master/audio%20spectrum_pt1_waveform_viewer.ipynb


import pyaudio
import os
import struct
import numpy as np
import matplotlib.pyplot as plt
import time
from Tkinter import TclError
import pyaudio
import sys


CHUNK = 1024 * 2             
FORMAT = pyaudio.paInt16     
CHANNELS = 1                 
RATE = 44100                 

def start():
    global pa

    fig, ax = plt.subplots(1, figsize=(15, 7))
    
    pa = pyaudio.PyAudio()
    stream = pa.open(
        format=FORMAT,
        channels=CHANNELS,
        rate=RATE,
        input=True,
        output=True,
        frames_per_buffer=CHUNK
    )
 
    
    x = np.arange(0, 2*CHUNK, 2)
    
    line, = ax.plot(x, np.random.rand(CHUNK), '-', lw=2)
    
    # basic formatting for the axes
    ax.set_title('Waveform')
    ax.set_xlabel('Samples')
    ax.set_ylabel('Amplitude')
    ax.set_ylim(0, 255)
    ax.set_xlim(0, CHUNK)
    plt.setp(ax, xticks=[0, CHUNK, 2*CHUNK], yticks=[-200, 0, 200])
    plt.show(block=False)
    
    stream.start_stream()
    print('stream started')
    
    while stream.is_active():
        data = stream.read(CHUNK)  

        data_int = struct.unpack(str(2 * CHUNK) + 'B', data) 
        data_np = np.array(data_int, dtype='b')[::2]
        
        line.set_ydata(data_np)

        try:
            fig.canvas.draw()
            fig.canvas.flush_events()
            
        except TclError:            
            print('stream stopped')
            break
        
    stream.stop_stream()
    stream.close()
    pa.terminate()
    
def main():
    start()


if __name__ == "__main__":
    main()    