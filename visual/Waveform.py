# code by markjay4k was used as an example: https://github.com/markjay4k/Audio-Spectrum-Analyzer-in-Python/blob/master/audio%20spectrum_pt1_waveform_viewer.ipynb

import pyaudio
import os
import struct
import numpy as np
import matplotlib.pyplot as plt
import time
from Tkinter import TclError
import pyaudio
import wave
import time
import sys

# samples per frame
CHUNK = 1024 * 2             

def formatBlock(data):
    count = len(data)/2
    format = "%dh"%(count)
    shorts = struct.unpack(format, data)
    return shorts


def callback(in_data, frame_count, time_info, status):
    global wf, pa, emit
    data = wf.readframes(frame_count)
    shorts = formatBlock(data)
    emit = shorts
    return (data, pyaudio.paContinue)


def start():
    global wf, pa

    fig, ax = plt.subplots(1, figsize=(15, 7))
    
    wf = wave.open('./resources/Esskeetit.wav', 'rb')
    pa = pyaudio.PyAudio()
    stream = pa.open(
        format=pa.get_format_from_width(wf.getsampwidth()),
        channels=wf.getnchannels(),
        rate=wf.getframerate(),
        #input=True,
        output=True,
        frames_per_buffer=CHUNK,
        #stream_callback=callback
    )
    print(wf.getsampwidth())
    
    x = np.arange(0, 2*3072, 2)
    
    line, = ax.plot(x, np.random.rand(3072), '-', lw=2)
    
    # basic formatting for the axes
    ax.set_title('Waveform')
    ax.set_xlabel('Samples')
    ax.set_ylabel('Amplitude')
    ax.set_ylim(0, 255)
    ax.set_xlim(0, 3072)
    plt.setp(ax, xticks=[0, 3072, 2*3072], yticks=[-200, 0, 200])
    plt.show(block=False)
    
    stream.start_stream()
    print('stream started')
    
    data = wf.readframes(CHUNK)
    
    while len(data) > 0:
        stream.write(data)
        data = wf.readframes(CHUNK)

        data_int = struct.unpack(str(6144) + 'B', data) 
        data_np = np.array(data_int, dtype='b')[::2]
        
        line.set_ydata(data_np)

        try:
            fig.canvas.draw_idle()
            fig.canvas.flush_events()
            
        except TclError:            
            print('stream stopped')
            break
        
    stream.stop_stream()
    stream.close()
    wf.close()
    pa.terminate()
    
def main():
    start()


if __name__ == "__main__":
    main()    